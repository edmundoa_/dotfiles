eval "`dircolors -b`"
alias ls='ls --color=auto'
alias tmux='TERM=xterm-256color /usr/bin/tmux'

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"

# Enable completions
fpath=(/etc/zsh-completions $fpath)
autoload -Uz compinit && compinit

if [ -x /usr/bin/javac ]; then
  export JAVA_HOME="$(readlink -f /usr/bin/javac | sed 's:/bin/javac::')"
fi

if [[ -z "$TMUX" ]] && [ "$SSH_CONNECTION" != "" ]; then
  tmux attach-session -t ssh_tmux || tmux new-session -s ssh_tmux
fi
