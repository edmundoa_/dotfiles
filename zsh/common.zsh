alias md5tree='find ./ -type f -print0 | xargs -0 md5sum'
alias ll='ls -l'
alias grep='egrep'
alias be='bundle exec'
alias bi='bundle install'
alias less='less -X'
alias sshu='ssh -o UserKnownHostsFile=/dev/null'
alias gpc='graylog-project'
alias gpu='graylog-project co -u'

export PATH="/usr/local/bin:/usr/local/sbin:$PATH"
export CDPATH=".:$HOME/Workspace/graylog/project:$HOME/Workspace/services"

if [ -d $HOME/Bin ]; then
  export PATH="$HOME/Bin:$PATH"
fi

# RVM
if [ -d "$HOME/.rvm" ]; then
  export PATH="$HOME/.rvm/bin:$PATH"
  [[ -s $HOME/.rvm/scripts/rvm ]] && source $HOME/.rvm/scripts/rvm
  [[ -r $rvm_path/scripts/completion ]] && source $rvm_path/scripts/completion
  rvm_silence_path_mismatch_check_flag=1
fi

# Rbenv
if [ -d "$HOME/.rbenv" ]; then
  eval "$(rbenv init -)" > /dev/null
fi

# Pyenv
if [ -d "$HOME/.pyenv" ]; then
  export PYENV_ROOT="$HOME/.pyenv"
  export PATH="$PYENV_ROOT/bin:$PATH"
  eval "$(pyenv init --path)"
  eval "$(pyenv init -)"
fi

if [ -d /usr/local/heroku ]; then
  export PATH="$PATH:/usr/local/heroku/bin"
fi

unset LC_ALL
unset LC_CTYPE
export LANG="en_US.UTF-8"

# Zsh configuration
setopt CORRECT
bindkey -v

# History customization
HISTFILE="$HOME/.zsh_history"
HISTSIZE=100000000
SAVEHIST=100000000
setopt EXTENDED_HISTORY
setopt SHARE_HISTORY
setopt APPEND_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_REDUCE_BLANKS
# Do not store entries starting with whitespace
setopt HIST_IGNORE_SPACE
# Perform history expansion and reload the line into the editing buffer
setopt HIST_VERIFY

# Improve search navigation
bindkey '^R' history-incremental-search-backward
bindkey $'^[[A' up-line-or-search
