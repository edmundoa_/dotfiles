alias mplayer="mplayer -framedrop -cache 8192"
alias cask='brew cask'
alias ia='open $1 -a /Applications/iA\ Writer.app'

notification() {
  title=$1
  message=$2
  osascript -e "display notification \"$message\" with title \"$title\""
}

# Enable completions
fpath=(/usr/local/share/zsh-completions $fpath)
autoload -Uz compinit && compinit

export PATH="$PATH:/usr/local/mysql/bin"
export PATH="$PATH:/usr/local/go/bin"

export CLICOLOR=1 # for terminal colors
export GREP_OPTIONS='--color=auto'
export KNIFE="env"

export JAVA_HOME=$(/usr/libexec/java_home)

if [ -f /usr/local/opt/android-sdk ]; then
  export ANDROID_HOME=/usr/local/opt/android-sdk
fi

export RUBY_CONFIGURE_OPTS="--with-openssl-dir=$(brew --prefix openssl@1.1)"

if [ -f /usr/local/bin/go ]; then
  export GOPATH=$HOME/Workspace/gopath
fi

export NVM_DIR="$HOME/.nvm"
if [ -f "$NVM_DIR/nvm.sh" ]; then
  source "$NVM_DIR/nvm.sh"
fi

eval "$(direnv hook zsh)"

