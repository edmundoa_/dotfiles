#### COLOUR (Solarized light)

# default statusbar colors
set-option -g status-style fg=brightred,bg=white

# default window title colors
set-window-option -g window-status-style fg=cyan,bg=default

# active window title colors
set-window-option -g window-status-current-style fg=white,bg=cyan

# pane border
set-option -g pane-border-style fg=brightwhite
set-option -g pane-active-border-style fg=brightblue

# message text
set-option -g message-style fg=red,bg=white

# pane number display
set-option -g display-panes-active-colour blue
set-option -g display-panes-colour brightred

# clock
set-window-option -g clock-mode-colour cyan

# bell
set-window-option -g window-status-bell-style fg=white,bg=red

