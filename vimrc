" Use vim settings instead of vi
set nocompatible

autocmd!

" Load plugins
call plug#begin('~/.vim/plugged')

Plug 'sheerun/vim-polyglot'
Plug 'bling/vim-airline'
Plug 'romainl/vim-cool'
Plug 'vim-airline/vim-airline-themes'
Plug 'scrooloose/nerdtree'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'tpope/vim-fugitive'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-dispatch'
Plug 'airblade/vim-gitgutter'
Plug 'mileszs/ack.vim'
Plug 'sheerun/vim-polyglot'
Plug 'dense-analysis/ale'

call plug#end()

" Load Man plugin
source $VIMRUNTIME/ftplugin/man.vim

" Enable syntax highlighting
syntax enable
if &term != 'linux'
  colorscheme default-light
endif

if exists("&colorcolumn")
  set colorcolumn=120
endif

" Enable filetype detection if it's supported
if has("autocmd")
  filetype on
  filetype plugin indent on

  autocmd FileType make setlocal ts=8 sts=8 sw=8 noexpandtab
  autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
  autocmd FileType python setlocal ts=4 sts=4 sw=4 expandtab
  autocmd FileType java setlocal ts=4 sts=4 sw=4 expandtab
  autocmd BufNewFile,BufRead *.less set filetype=less.css

  autocmd FileType gitcommit setlocal ts=4 sts=4 sw=4 expandtab
  autocmd FileType markdown,text,gitcommit,"" setlocal spell
endif

" GUI tweaks
if has("gui_running")
  set guioptions=t "hide MacVim toolbar
  set guifont=Fira\ Code:h13
endif

" Statusline tricks
set laststatus=2
set noru
set noshowmode

" vim-airline config
let g:airline_theme='bubblegum'
let g:airline_enable_syntastic=0
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled = 1

" Change split behavior
set splitright
set splitbelow

" Set colors
highlight LineNr ctermfg=darkgrey
set cursorline
highlight NonText ctermfg=darkgrey
highlight SpecialKey ctermfg=darkgrey
highlight clear SignColumn
highlight link SignColumn LineNr
highlight SpellBad ctermbg=lightred guibg=#fed7d7

" Use 2 spaces instead of tabs
set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=2

" Improve search
set ignorecase
set smartcase
set hlsearch
set incsearch

" Backup and swap options
set nobackup
set writebackup
set swapfile

" Print hidden characters
set list
set lcs=tab:»»,eol:¬

" Show available command options when tab is pressed
set wildmode=longest,list,full
set wildmenu

" Other options
set enc=utf-8
set number
set whichwrap+=>
set whichwrap+=<
set browsedir=current
set wrap
set hidden
set nostartofline
set history=5000
set backspace=indent,eol,start
set ruler
set nomodeline
set scrolloff=3

" Open Nerdtree function (In dir of opened file)
function! OpenNERDTree()
  execute ":NERDTree %"
endfunction
command! -nargs=0 OpenNERDTree :call OpenNERDTree()

" Toggle paste mode
set pastetoggle=<Leader>p

" Disable arrow keys
map <Left> <Nop>
map <Right> <Nop>
map <Up> <Nop>
map <Down> <Nop>

" Customize CtrlP
nmap ,f :CtrlP<CR>
nmap ,h :CtrlPCurFile<CR>
nmap ,m :CtrlPMRUFiles<CR>

let g:ctrlp_max_files=20000
let g:ctrlp_working_path_mode = 'a'
let g:ctrlp_custom_ignore = '\v[\/](node_modules|target|dist)|(\.(swp|ico|git|svn))$'

" Open :Copen! with ,c
nmap ,c :Copen!<CR>

" Open NERDTree with ,n
nmap ,n :OpenNERDTree<CR>

" Relative line numbering quick keys.
nmap ,l :RN<CR>

" Mapping to open file under cursor
nmap ,o  gf<CR>
nmap ,ov :vertical wincmd f<CR>
nmap ,ot :wincmd gf<CR>

" Mappings to align code
vmap <Leader>ae :Tabularize /=<CR>
vmap <Leader>aa :Tabularize /=><CR>
vmap <Leader>a: :Tabularize /:\zs<CR>
vmap <Leader>a, :Tabularize /,\zs<CR>

nnoremap <Leader>a <C-a>
nnoremap <Leader>x <C-x>

vmap <Leader>c :w !pbcopy<CR><CR>
nmap <Leader>c :.w !pbcopy<CR><CR>

" Use The Silver Searcher as search
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" Customize ale
let g:ale_fixers = {
\  'javascript': ['eslint'],
\  'javascriptreact': ['eslint'],
\  'typescript': ['eslint'],
\  'typescript.tsx': ['eslint']
\}
let g:ale_fix_on_save = 1
let g:ale_lint_on_text_changed = 'normal'
let g:ale_lint_delay = 0
let g:ale_set_loclist = 0
let g:ale_set_highlights = 0
nnoremap gk :ALEPreviousWrap<CR>
nnoremap gj :ALENextWrap<CR>
nnoremap g= :ALEFix<CR>

